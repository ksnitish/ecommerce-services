const mongoose = require('mongoose');
const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb+srv://admin:root@freecluster.hz8qi.mongodb.net/demo?ssl=true';
mongoose.connect(url, {useNewUrlParser: true, useFindAndModify: true, useCreateIndex:true, useUnifiedTopology: true});
var db=mongoose.connection; 

db.once('open', function(){
    console.log('Done..');
}).on('error', function(error){
    console.log('Error', error);
});

module.exports = {
    getSales:  function(req, res){
      db.collection("user").findOne({}, function(err, result) {
         if (err) throw err;
         res.status(200).send(result);
         db.close();
        });
    }
};
