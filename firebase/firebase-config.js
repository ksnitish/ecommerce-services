const firebase = require("firebase");

const app = firebase.initalizeApp({
    apiKey:"AIzaSyCK08pKY339VxpM3flBx7LaOMASAPyfbVg",
    authDomain:"fir-project-9afa6.firebase.com",
    databaseURL:"https://fir-project-9afa6.firebaseio.com/"
}); 

module.exports = app;
