
const User = require('../model/user.model');
const bcrypt = require('bcryptjs');

exports.userRegistration = async function(req, res){
    var { username, password, phone } = req.body;
    if(!username || typeof username !== 'string') return res.json({status: 'error', error: 'Invalid Username'});
    if(!password || typeof password !== 'string') return res.json({status: 'error', error: 'Invalid password'});
    if(!phone || typeof phone !== 'number') return res.json({status: 'error', error: 'Invalid phone'});
    password = await bcrypt.hash(password, 13);
    try{
      const repsonse = await User.insertMany({
        username,
        password,
        phone
      });
    }catch(error){
      if(error.code === 11000){
        return res.json({status: 'error', error: 'Username already exists.'});
      }
      throw error;
    }
    res.json({status: 'ok'});
}