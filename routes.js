const routes = require('express').Router();
const userController = require('./controller/user-controller');
var dbOperation = require('./db/connection');
const User = require('./model/user.model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const JWT_SECRET = 'dsdsdsdfefre32eww2r@#!#FDfdfdfdf';

routes.get('/demo', function (req, res) {
    dbOperation.getSales(req, res);
 });

 routes.post('/api/login', async (req, res) =>{
    const {username, password} = req.body;
    const user = await User.findOne({username}).lean();
    if(!user) return res.json({status: 'error', error: 'Invalid Username/Password.'})
    if(await bcrypt.compare(password, user.password)){
      // validation true
      const token = jwt.sign({id: user._id, username: user.username}, JWT_SECRET);
      return res.json({status: 'ok', data: token, username: user.username})
    }
    res.json({status: 'error', error: 'Invalid Username/Password.'})
  
  });
  
  routes.post('/api/register',(req, res) => {
    userController.userRegistration(req,res);
  });

 module.exports = routes;