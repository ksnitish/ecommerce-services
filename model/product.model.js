// manganing dataflow in easy way
// inforce shema for db

const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
        name:{type: String},
        category:{type: String},
        description:{type: String},
        short_description:{type: String},
        price:{type: Number},
        special_price:{type: Number},
        base_image:{data: Buffer, contentType: String},
        total_qty:{type: Number},
        isSellingFast:Boolean,
        size:{type: Number},
        created_at:{ type: Date, default: Date.now },
        updated_at:{ type: Date, default: Date.now }
    
},{
    collection: 'product'
}
)

const model = mongoose.model('ProductSchema', ProductSchema);

module.exports = model;
