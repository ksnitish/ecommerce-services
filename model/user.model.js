// manganing dataflow in easy way
// inforce shema for db

const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
     username: {type: String, required: true, unique: true},
     password: {type: String, required: true, unique: true}, 
     phone: {type: Number, required: true, unique: true}
},{
    collection: 'user'
}
)

const model = mongoose.model('UserSchema', UserSchema);

module.exports = model;
