var express = require('express');
const routes = require('./routes');
const cors = require('cors');
const { response } = require('express');

var app = express();
app.use(cors());
app.use(express.json({
    inflate: true,
    limit: '100kb',
    reviver: null,
    strict: true,
    type: 'application/json',
    verify: undefined,
    cors:true
  }));

app.use('/', routes);  

app.listen(8089, function () {
    console.log('Listening to Port 8089');
  });
